import 'package:flutter/material.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';
import 'package:yellowflock/Design/Homepage.dart';
import 'package:yellowflock/Design/MyCart.dart';
import 'package:yellowflock/Design/profile.dart';
import 'package:yellowflock/Design/search.dart';

class BottomNav extends StatefulWidget {
  @override
  _BottomNavState createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
  int _selectedIndex = 0;

  List<Widget> kk = [
    HomePage(),
    Mycart(),
    Search(),
    Profile(),
  ];

  void ontap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  var _selectedTab = _SelectedTab.home;

  void _handleIndexChanged(int index) {
    setState(() {
      _selectedTab = _SelectedTab.values[index];
    });
  }

  @override
  Widget build(BuildContext context) {
    Scaffold(
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.only(bottom: 20, left: 15, right: 15),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(11),
              color: Colors.blue.shade50,
            ),
            height: 60,
            child: SalomonBottomBar(
              items: [
                /// Home
                SalomonBottomBarItem(
                  icon: Icon(
                    Icons.home_filled,
                    size: 25,
                  ),
                  title: Text("Home",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 17)),
                  selectedColor: Colors.indigo.shade900,
                  unselectedColor: Colors.black38,
                ),

                /// Likes
                SalomonBottomBarItem(
                  icon: Icon(
                    Icons.search_sharp,
                    size: 25,
                  ),
                  title: Text("Search",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 17)),
                  selectedColor: Colors.indigo.shade900,
                  unselectedColor: Colors.black38,
                ),

                /// Search
                SalomonBottomBarItem(
                  icon: Icon(
                    Icons.shopping_bag_outlined,
                    size: 25,
                  ),
                  title: Text("Cart",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 17)),
                  selectedColor: Colors.indigo.shade900,
                  unselectedColor: Colors.black38,
                ),

                /// Profile
                SalomonBottomBarItem(
                  icon: Icon(
                    Icons.person,
                    size: 25,
                  ),
                  title: Text(
                    "Profile",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                  ),
                  selectedColor: Colors.indigo.shade900,
                  unselectedColor: Colors.black38,
                ),
              ],
              currentIndex: _selectedIndex,
              onTap: ontap,
            ),
          ),
        ),
        body: kk[_selectedIndex]);
  }
}

enum _SelectedTab { home, search, cart, profile }
