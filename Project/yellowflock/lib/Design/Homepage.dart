import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:yellowflock/Design/MyCart.dart';
import 'package:yellowflock/Design/Notification.dart';
import 'package:yellowflock/Design/combo.dart';
import 'package:yellowflock/Design/favourite.dart';
import 'package:yellowflock/Design/myorder.dart';
import 'package:yellowflock/Design/topdeals.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    ///Carousel

    Widget image_slider_carousel = Container(
      height: 220,
      width: MediaQuery.of(context).size.width,
      child: Carousel(
        boxFit: BoxFit.fill,
        images: [
          Image.asset(
            "asset/banner.png",
            fit: BoxFit.fill,
          ),
          Image.asset(
            "asset/offer3.jfif",
            fit: BoxFit.fill,
          ),
        ],
        autoplay: true,
        indicatorBgPadding: 0,
        dotBgColor: Colors.white,
        dotColor: Colors.black,
        dotSize: 5.0,
        showIndicator: true,
        dotIncreasedColor: Colors.yellow,
        dotSpacing: 8,
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      drawer: Drawer(
        child: ListView(
          children: [
            Container(
                height: 65,
                color: Colors.indigo[900],
                child: ListTile(
                  leading: Icon(
                    Icons.menu,
                    size: 40,
                    color: Colors.white70,
                  ),
                  title: Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Delivery Location",
                          style: TextStyle(fontSize: 10, color: Colors.white),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5),
                          child: Row(
                            children: [
                              Text(
                                "Kozhikode",
                                style: TextStyle(
                                    fontSize: 12, color: Colors.white),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Icon(
                                Icons.edit_outlined,
                                color: Colors.white,
                                size: 15,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Combo()));
              },
              child: Container(
                height: 55,
                color: Colors.white,
                child: ListTile(
                  leading: Icon(Icons.local_offer_outlined),
                  title: Text("Combo Offer"),
                ),
              ),
            ),
            Divider(
              height: 4,
              thickness: 4,
              color: Colors.black12,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Topdeals()));
              },
              child: Container(
                height: 55,
                color: Colors.white,
                child: ListTile(
                  leading: Icon(Icons.filter_vintage_sharp),
                  title: Text("Top Deals"),
                ),
              ),
            ),
            Divider(height: 3, thickness: 3, color: Colors.black12),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Myorders()));
              },
              child: Container(
                height: 55,
                color: Colors.white,
                child: ListTile(
                  leading: Icon(Icons.book_outlined),
                  title: Text("My Order"),
                ),
              ),
            ),
            Divider(
              height: 3,
              thickness: 3,
              color: Colors.black12,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Favourite()));
              },
              child: Container(
                height: 55,
                color: Colors.white,
                child: ListTile(
                  leading: Icon(Icons.favorite_border),
                  title: Text("My Favourites"),
                ),
              ),
            ),
            Divider(
              height: 3,
              thickness: 3,
              color: Colors.black12,
            ),
            Container(
              height: 55,
              color: Colors.white,
              child: ListTile(
                leading: Icon(Icons.account_balance_wallet),
                title: Text("My Wallet"),
              ),
            ),
            Divider(
              height: 3,
              thickness: 3,
              color: Colors.black12,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Mycart()));
              },
              child: Container(
                height: 55,
                color: Colors.white,
                child: ListTile(
                  leading: Icon(Icons.shopping_cart_rounded),
                  title: Text("My Cart"),
                ),
              ),
            ),
            Divider(height: 3, thickness: 3, color: Colors.black12),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => noti()));
              },
              child: Container(
                height: 55,
                color: Colors.white,
                child: ListTile(
                  leading: Icon(Icons.notifications_none_outlined),
                  title: Text("Notification"),
                ),
              ),
            ),
            Divider(height: 3, thickness: 3, color: Colors.black12),
            Container(
              height: 65,
              color: Colors.white,
              child: ListTile(),
            ),
            Divider(height: 3, thickness: 3, color: Colors.black12),
            Container(
              color: Colors.white,
              child: ListTile(
                leading: Icon(Icons.share),
                title: Text("Share with Friends"),
              ),
            ),
            Container(
              color: Colors.white,
              child: ListTile(
                leading: Icon(Icons.star),
                title: Text("Rate us on PlayStore"),
              ),
            ),
            Container(
              color: Colors.white,
              child: ListTile(
                leading: Icon(Icons.security),
                title: Text("Contact & Support"),
              ),
            ),
          ],
        ),
      ),
      appBar: AppBar(
        toolbarHeight: 80,
        title: FlatButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            splashColor: Colors.grey[200],
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  "Kozhikode",
                  style: TextStyle(fontSize: 12, color: Colors.white),
                ),
                Icon(
                  Icons.location_on_outlined,
                  color: Colors.white,
                  size: 23,
                ),
              ],
            ),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return abc('kozhikode');
                  });
            }),
        actions: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 10, top: 5),
                child: Container(
                  height: 35,
                  width: 180,
                  child: TextField(
                    decoration: InputDecoration(
                      hintText: "Search here..",
                      hintStyle: TextStyle(color: Colors.black, fontSize: 11),
                      suffixIcon: Icon(
                        Icons.search_rounded,
                        size: 20,
                        color: Colors.black,
                      ),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(9),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(9),
                          borderSide: BorderSide(color: Colors.white)),
                      filled: true,
                      fillColor: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
        backgroundColor: Colors.indigo[900],
      ),
      body: ListView(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        children: [
          /// Category Bar

          Column(
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 60,
                child: ListView(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  physics: ScrollPhysics(),
                  children: [
                    Row(
                      children: [
                        Container(
                          child: Center(
                              child: Text(
                            "Grocery",
                            style: TextStyle(color: Colors.black),
                          )),
                          color: Colors.grey[400],
                          height: 50,
                          width: 100,
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          child: Center(
                              child: Text(
                            "Vegitables",
                            style: TextStyle(color: Colors.black),
                          )),
                          color: Colors.grey[400],
                          height: 50,
                          width: 100,
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          child: Center(
                              child: Text(
                            "Meat",
                            style: TextStyle(color: Colors.black),
                          )),
                          color: Colors.grey[400],
                          height: 50,
                          width: 100,
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          child: Center(
                              child: Text(
                            "Bevarages",
                            style: TextStyle(color: Colors.black),
                          )),
                          color: Colors.grey[400],
                          height: 50,
                          width: 100,
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          child: Center(
                              child: Text(
                            "Grocery",
                            style: TextStyle(color: Colors.black),
                          )),
                          color: Colors.grey[400],
                          height: 50,
                          width: 100,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),

          ///Banner image

          Column(
            children: <Widget>[
              image_slider_carousel,
            ],
          ),

          ///Top Deals

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: Row(
                children: [
                  Icon(
                    Icons.circle,
                    color: Colors.blue[50],
                  ),
                  Text(
                    " Top Deals",
                    style: TextStyle(fontSize: 22, color: Colors.black87),
                  )
                ],
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.all(9.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 170,
                  width: MediaQuery.of(context).size.width,
                  child: ListView(
                    // shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    physics: ScrollPhysics(),
                    children: [
                      Container(
                        height: 150,
                        width: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.zero,
                        ),
                        child: Column(
                          children: [
                            Image.asset(
                              "asset/shmpoo.jfif",
                              height: 100,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Text("Shamboo "),
                                  Icon(
                                    Icons.favorite_border,
                                    size: 15,
                                    color: Colors.black45,
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 18),
                                  child: Text('\u{20B9}' + "148"),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('\u{20B9}' + "148"),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 150,
                        width: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.zero,
                        ),
                        child: Column(
                          children: [
                            Image.asset(
                              "asset/KLAR.jpg",
                              height: 100,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("KLAR "),
                                  Icon(
                                    Icons.favorite_border,
                                    size: 15,
                                    color: Colors.black45,
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 18),
                                  child: Text('\u{20B9}' + "148"),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('\u{20B9}' + "148"),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 150,
                        width: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.zero,
                        ),
                        child: Column(
                          children: [
                            Image.asset(
                              "asset/shmpoo.jfif",
                              height: 100,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Text("Shamboo "),
                                  Icon(
                                    Icons.favorite_border,
                                    size: 15,
                                    color: Colors.black45,
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 18),
                                  child: Text('\u{20B9}' + "148"),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('\u{20B9}' + "148"),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 150,
                        width: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.zero,
                        ),
                        child: Column(
                          children: [
                            Image.asset(
                              "asset/KLAR.jpg",
                              height: 100,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Text("KLAR"),
                                  Icon(
                                    Icons.favorite_border,
                                    size: 15,
                                    color: Colors.black45,
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 18),
                                  child: Text('\u{20B9}' + "148"),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('\u{20B9}' + "148"),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 150,
                        width: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.zero,
                        ),
                        child: Column(
                          children: [
                            Image.asset(
                              "asset/shmpoo.jfif",
                              height: 100,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Text("Shamboo "),
                                  Icon(
                                    Icons.favorite_border,
                                    color: Colors.black45,
                                    size: 15,
                                  )
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 18),
                                  child: Text('\u{20B9}' + "148"),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('\u{20B9}' + "148"),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

          /// Card

          Row(
            children: [
              Expanded(
                child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Container(
                      height: 170,
                      width: MediaQuery.of(context).size.width,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.asset('asset/card (2).png',
                            fit: BoxFit.cover),
                      ),
                    )),
              ),
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Container(
                    height: 170,
                    width: 120,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.asset('asset/card 3.png', fit: BoxFit.fill),
                    ),
                  )),
            ],
          ),

          /// Offer

          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Container(
              //height: 150,
              width: MediaQuery.of(context).size.width,
              child: Image.asset(
                'asset/offer4.png',
                fit: BoxFit.fill,
              ),
            ),
          ),

          /// Combo Offer

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: Row(
                children: [
                  Icon(Icons.circle, color: Colors.blue[50]),
                  Text(
                    " Combo Offer",
                    style: TextStyle(fontSize: 22, color: Colors.black87),
                  )
                ],
              ),
            ),
          ),

          ///Product

          Container(
            color: Colors.grey[200],
            height: 170,
            width: MediaQuery.of(context).size.width,
            child: ListView(
                // shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                physics: ScrollPhysics(),
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 5, right: 3, top: 5, bottom: 13),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Container(
                        height: 120,
                        width: 260,
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(6),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 10,
                                  spreadRadius: 1)
                            ]),
                        child: Stack(
                          children: [
                            Row(
                              children: [
                                Image.asset('asset/pepsi.jpg'),
                                Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 35,
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text("Pepsi 3l"),
                                          SizedBox(
                                            height: 3,
                                          ),
                                          Text("buy 1 get"),
                                          SizedBox(
                                            height: 3,
                                          ),
                                          Text("Cocacola 750 ml"),
                                          SizedBox(
                                            height: 4,
                                          ),
                                          Text("Rs: 148"),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                            Positioned(
                              right: 0,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(4),
                                child: Container(
                                  height: 40,
                                  width: 40,
                                  color: Colors.redAccent[100],
                                  child: Icon(
                                    Icons.favorite_border,
                                    size: 25,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 5, right: 5, top: 5, bottom: 13),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Container(
                        height: 120,
                        width: 260,
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 10,
                                  spreadRadius: 5)
                            ]),
                        child: Stack(
                          children: [
                            Row(
                              children: [
                                Image.asset('asset/pepsi.jpg'),
                                Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 35,
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text("Pepsi 3l"),
                                          SizedBox(
                                            height: 3,
                                          ),
                                          Text("buy 1 get"),
                                          SizedBox(
                                            height: 3,
                                          ),
                                          Text("Cocacola 750 ml"),
                                          SizedBox(
                                            height: 4,
                                          ),
                                          Text("Rs: 148"),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                            Positioned(
                              right: 0,
                              height: 40,
                              width: 40,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(3),
                                child: Container(
                                  color: Colors.redAccent[100],
                                  child: Icon(
                                    Icons.favorite_border,
                                    size: 25,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ]),
          ),
        ],
      ),
    );
  }
}

class abc extends StatelessWidget {
  final title;
  abc(this.title);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        right: 80,
        top: 60,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          AlertDialog(
            title: Text(
              "Choose Nearest Store",
              style: TextStyle(fontSize: 18, color: Colors.white),
            ),
            content: Container(
                height: MediaQuery.of(context).size.height * 0.03,
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.grey[600])),
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 5,
                    top: 4,
                  ),
                  child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        title,
                        style: TextStyle(fontSize: 15, color: Colors.white),
                      )),
                )),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
              side: BorderSide(width: 1, color: Colors.grey[600]),
            ),
            backgroundColor: Colors.indigo[900],
          ),
        ],
      ),
    );
  }
}
